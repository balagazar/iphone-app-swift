//
//  Movie.swift
//  Movie Tracker
//
//  Created by Student on 2017-12-16.
//  Copyright © 2017 Mohawk College. All rights reserved.
//

import UIKit

class Movie: NSObject, NSCoding {
    
    //properties
    var title : String
    var viewedDate: Date
    var theatre : String
    var rating: Int?{
        didSet {
            if rating! > 10
            {
                rating = nil
                
            }else if rating! < 1 {
                rating = nil
            }
        }
    }
    
    //encode to archive
    func encode(with aCoder: NSCoder) {
        
        if rating == nil{
            aCoder.encode(0, forKey: "rating")
        }else{
            aCoder.encode(rating, forKey: "rating")
        }
        aCoder.encode(title, forKey: "title")
        aCoder.encode(viewedDate, forKey: "viewedDate")
        aCoder.encode(theatre, forKey: "theatre")
        
    }
    
    //decode when archiving
    required init?(coder aDecoder: NSCoder) {
        title = aDecoder.decodeObject(forKey: "title") as! String
        viewedDate = aDecoder.decodeObject(forKey: "viewedDate") as! Date
        theatre = aDecoder.decodeObject(forKey: "theatre") as! String
        rating = aDecoder.decodeObject(forKey: "rating") as! Int!
        
        if rating == 0
        {
            rating = nil
        }
        
        super.init()
    }
    //constructor
    init(title:String, viewedDate:Date, theatre:String,rating:Int?)
    {
        self.title = title
        self.viewedDate = Date()
        self.theatre = theatre
        self.rating = rating
        
        super.init()
    }
    
    
    convenience init(Create: Bool = false) {
        if Create {
            
            self.init(title:"", viewedDate:Date(), theatre:"",rating:nil)
        } else {
            self.init(title:"", viewedDate:Date(), theatre:"",rating:0)
        }
    }
}
