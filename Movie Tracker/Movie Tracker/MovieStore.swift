//
//  MovieStore.swift
//  Movie Tracker
//
//  Created by Student on 2017-12-16.
//  Copyright © 2017 Mohawk College. All rights reserved.
//

import UIKit

class MovieStore{
    //array that holds Movies (objects)
    var allMovies = [Movie]()
    
    //create new movie and return it as object(Movie)
    @discardableResult func createNewMovie() -> Movie{
        let newMovie = Movie(Create: true)
        
        allMovies.append(newMovie)
        
        return newMovie
    }
    
    
    //Create url where saved archives is stored
    let URL: URL = {
        let documentsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = documentsDirectories.first!
        return documentDirectory.appendingPathComponent("Movies.archive")
    }()
    
    //Save the data in the URL when closing app
    func saveChanges() -> Bool {
        print("Saving items to: \(URL.path)")
        return NSKeyedArchiver.archiveRootObject(allMovies, toFile: URL.path)
    }
    
    //removing the moving that is passed in by finding it index
    func removeMovie(_ movie:Movie){
        if let index = allMovies.index(of: movie){
            allMovies.remove(at: index)
        }
    }
    
    
    //moving function removes the row from one cell and creates in the chosen cell.
    func moveMovie(from fromIndex: Int, to toIndex: Int) {
        if fromIndex == toIndex {
            return
        }
        
        // Get reference to object being moved so you can reinsert it
        let movedMovie = allMovies[fromIndex]
        
        // Remove movie from array
        allMovies.remove(at: fromIndex)
        
        // Insert movie in array at new location
        allMovies.insert(movedMovie, at: toIndex)
    }
    
    //when app starts, it grabs the archvies files which holds the movie and assigns them to be the new allMovie array
    init() {
        if let archivedMovies = NSKeyedUnarchiver.unarchiveObject(withFile: URL.path) as? [Movie] {
            allMovies = archivedMovies
        }
    }
    
}
