//
//  MoviesViewController.swift
//  Movie Tracker
//
//  Created by Student on 2017-12-16.
//  Copyright © 2017 Mohawk College. All rights reserved.
//


//I, Alexander Logvinenko, student number 000346587, certify that this material is my original work.
//No other person's work has been used without due acknowledgement and I have not made my work available to anyone else.
import UIKit

class MoviesViewController : UITableViewController{
    
    //Globar indexpath used when creating new movie
    var indexPath : IndexPath!
    //Creating class movieStore
    var movieStore = MovieStore()
    
    //Count how many movies I have in the allMovies array inside the movieStore Class
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)->Int{
        return movieStore.allMovies.count
    }
    
    //create cells in the tableview, each cell will have its own color image
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        
        // Set the text on the cell with the description of the item
        // that is at the nth index of items, where n = row this cell
        // will appear in on the tableview
        let movie = movieStore.allMovies[indexPath.row]
        
        let rating = movie.rating
        
        if rating == nil || rating! == 0{
            cell.imageView?.image = #imageLiteral(resourceName: "blank")
        }else if rating! >= 1 && rating!  < 4{
            cell.imageView?.image = #imageLiteral(resourceName: "red")
        }else if rating! >= 4 && rating!  < 7{
            cell.imageView?.image = #imageLiteral(resourceName: "yellow")
        }else if rating! >= 7 && rating!  <= 10{
            cell.imageView?.image = #imageLiteral(resourceName: "green")
        }
        cell.textLabel?.text = movie.title
        
        return cell
    }
    
    //This function connected to the plux tab bar button, which class the function createNewMovie in movie store and create a new movie
    @IBAction func AddMovie(_ sender: UIBarButtonItem) {
        
        // Create a new Movie and add it to the store
        let newMovie = movieStore.createNewMovie()
        
        // Figure out where that movie is in the array
        if let index = movieStore.allMovies.index(of: newMovie) {
            indexPath = IndexPath(row: index, section: 0)
            
            // Insert this new row into the table
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
    }
    
    //Set a row height
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tableView.reloadData()
    }
    
    //Create edit button in the navigation controll
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        navigationItem.leftBarButtonItem = editButtonItem
    }
    //Delete row
    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCellEditingStyle,
                            forRowAt indexPath: IndexPath) {
        // If the table view is asking to commit a delete command...
        if editingStyle == .delete {
            let movie = movieStore.allMovies[indexPath.row]
            // Remove the movie from the movie store
            movieStore.removeMovie(movie)
            
            // Also remove that row from the table view with an animation
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    //Call a function move Movie in movieStore that relocates the rows.
    override func tableView(_ tableView: UITableView,
                            moveRowAt sourceIndexPath: IndexPath,
                            to destinationIndexPath: IndexPath) {
        // Update the model
        movieStore.moveMovie(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    //This function prepare to change to the Detail view controller. If the segue (plus button/add new move) button clicked, create a new movie and go update it.
    //if a row segue clicked, go to edit and view that Movie detail
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "showMovie" segue
        switch segue.identifier {
        case "showMovie"?:
            // Figure out which row was just tapped
            if let row = tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let movie = movieStore.allMovies[row]
                let detailViewController = segue.destination as! DetailViewController
                detailViewController.movie = movie
            }
        // If the triggered segue is the "newMovie" segue
        case "newMovie"?:
            let movie = movieStore.createNewMovie()
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.movie = movie
        default:
            preconditionFailure("Unexepected segue identifier.")
        }
    }
}
