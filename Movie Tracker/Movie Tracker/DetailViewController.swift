//
//  DetailViewController.swift
//  Movie Tracker
//
//  Created by Student on 2017-12-16.
//  Copyright © 2017 Mohawk College. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITextFieldDelegate {
    
    //text fields
    @IBOutlet var titleField: UITextField!
    @IBOutlet var TheatreField: UITextField!
    @IBOutlet var RatingField: UITextField!
    @IBOutlet var DatePicker: UIDatePicker!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //When click on the background while number/text pad is open, closes the pad using tapGesture recognizer
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    //number formater
    let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    //date formatter
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
    
    //create class movie, and check if the title is emopty, if it is, sets the navigation tilte to be "New movie" otherwise sets the name of the movie
    var movie: Movie!{
        didSet {
            if movie.title == ""
            {
                navigationItem.title = "New Movie"
                
            }else{
                navigationItem.title = movie.title
            }
        }
    }
    
    //when view appears sets all the text fields to be the movie info(properties)
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        titleField.text = movie.title
        TheatreField.text = movie.theatre
        
        if movie.rating != nil{
            RatingField.text = "\(movie.rating!)"
        }else if movie.rating == 0{
            RatingField.text = ""
        }else{
            RatingField.text = ""
        }
        DatePicker.date = movie.viewedDate
    }
    
    //When going back saves the changes in the movie 
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        view.endEditing(true)
        movie.viewedDate = DatePicker.date
        
        movie.title = titleField.text ?? ""
        movie.theatre = TheatreField.text!
        
        if let valueText = RatingField.text,
            let value = numberFormatter.number(from: valueText) {
            movie.rating = value.intValue
        } else {
            movie.rating = 0
        }
        
    }
}
